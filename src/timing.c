/**
 * timing.c
 * timing functions
 */

#include "timing.h"

#define EXECUTING_FROM_RAM 1

static void nop()
{
#ifdef EXECUTING_FROM_RAM
	__asm__ (
		"nop		\n"
	);
#endif
}

void delay(int size)
{
	unsigned int  i;

	for (i=0;i<size;i++)
		nop();
}
