/**
 * serial.c
 * serial support
 */

#include "arch.h"
#include "serial.h"

#include "config.h"

#if MCF_CLK == 40000000
#define TICKS_TIMEOUT 0x600000
#endif
#if MCF_CLK == 45000000
#define TICKS_TIMEOUT 0x700000
#endif

void init_serial(unsigned int uart_base, unsigned int speed)
{
	volatile unsigned char  *uartp;
	unsigned short divider;

	/*
	* Reset UART, get it into known state...
	*/
	uartp = (volatile unsigned char *) (MCF_MBAR + uart_base);

	uartp[MCFUART_UCR] = MCFUART_UCR_CMDRESETRX;    /* reset RX */
	uartp[MCFUART_UCR] = MCFUART_UCR_CMDRESETTX;    /* reset TX */
	uartp[MCFUART_UCR] = MCFUART_UCR_CMDRESETMRPTR; /* reset MR pointer */
	uartp[MCFUART_UCR] = MCFUART_UCR_CMDRESETERR;   /* reset Error pointer */

	/*
	* Set port for specified baud rate, 8 data bits, 1 stop bit, no parity.
	*/
	uartp[MCFUART_UMR] = MCFUART_MR1_PARITYNONE | MCFUART_MR1_CS8;
	uartp[MCFUART_UMR] = MCFUART_MR2_STOP1;

	/*
	* Calculate baud settings
	*/
	divider = (unsigned short)((unsigned long)MCF_CLK /
		(unsigned long)(32 * speed));

	/* rounding, for some clock speed only (40 Mhz) */
	//if (speed==115200) divider++;

	// BAUD RATE : set-up divider register
	uartp[MCFUART_UBG1] = (((int)divider >> 8) & 0xFF);
	uartp[MCFUART_UBG2] = ((int)divider & 0x00FF);

	/*  Note: This register is not in the 5249 docs. I am assuming that Greg knew
	 *        what he was doing, so I'm leaving it here.  ~Jeremy */
	//uartp[MCFUART_UFPD] = ((int)fraction & 0xf);  /* set baud fraction adjust */

	uartp[MCFUART_UCSR] = MCFUART_UCSR_RXCLKTIMER | MCFUART_UCSR_TXCLKTIMER;
	uartp[MCFUART_UCR]  = MCFUART_UCR_RXENABLE | MCFUART_UCR_TXENABLE;
}

/** output on the console port **/
void putchar(unsigned char ch)
{
	volatile unsigned char *uartp;
	int i;

	uartp = (volatile unsigned char *)(MCF_MBAR + UART_CONSOLE);

	while (!(uartp[MCFUART_USR] & MCFUART_USR_TXREADY));

        uartp[MCFUART_UTB] = ch;
}

int getchar(unsigned char timeout)
{
        int ticks=0;
        volatile unsigned char *uartp;

        uartp = (volatile unsigned char *)(MCF_MBAR + UART_CONSOLE);

        while (!(uartp[MCFUART_USR] & MCFUART_USR_RXREADY))
        {
                if (timeout)
                {
                        if (++ticks>=TICKS_TIMEOUT) return GETCHAR_TIMEOUT;
                }
        }
        return(uartp[MCFUART_URB]);
}

static void write_hex_nibble (unsigned char n)
{
        putchar(n+((n<10)?48:87));
}

void write_hex_b(unsigned char c)
{
        write_hex_nibble(c/16);
        write_hex_nibble(c%16);
}

void write_hex_s(unsigned short s)
{
        write_hex_b(s>>8);
        write_hex_b(s&0xff);
}

void write_hex_l(unsigned long l)
{
        write_hex_b(l>>24);
        write_hex_b((l>>16)&0xff);
        write_hex_b((l>>8)&0xff);
        write_hex_b(l&0xff);
}

void write(unsigned char *p)
{
	while (*p != 0) putchar(*p++);
}

void itoa(unsigned char *p, unsigned int n)
{
	int	q=n,i=0;

	while(q/10) {q/=10;i++;}
	p[i+1]=0;
	do {
		p[i]=n%10+0x30;
		n/=10;
	} while(i--);
}

void writen(int v)
{
	static char buff[10];

	itoa  (buff,v);
	write (buff);
}

