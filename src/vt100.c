/*
 * vt100.c
 * vt100 support
 */

#include "vt100.h"
#include "serial.h"

#ifndef DEBUG
void vt100_screen_clear()
{
	write("\x1b[2J");
}

void vt100_cursor_home()
{
	write("\x1b[0;0H");
}

void vt100_cursor_pos(int col, int row)
{
	write("\x1b[");
	writen(++row);
	write(";");
	writen(++col);
	write("f");
}

void vt100_write_color(color c, unsigned char *text)
{
	write(c);
	write(text);
}
#endif
