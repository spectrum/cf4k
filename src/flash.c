/*
 * flash.c
 *
 * flash memory debug and management
 *
 */

#include "flash.h"
#include "arch.h"
#include "timing.h"
#include "serial.h"

#include "config.h"

#define __BE(x)  		(x<<8) // translate to big endian

#ifdef  SST39VF320xB
#define BLOCK			(1024x64)
#endif

volatile unsigned short *baseaddress;

void fl_dump(volatile unsigned short *addr, int size)
{
	volatile unsigned short *fp = addr;
	int x, y;

	size >>= 3;

	*baseaddress = (unsigned short)0xF0F0;

	write ("\r\n");
	for (y = 0; y < size; ++y) {
		write_hex_s(y << 4);
		write(":");
		for (x = 0; x < 8; ++x, fp++) {
			write(" ");
			write_hex_s(*fp);
		}
		write("\r\n");
	}
}

static inline void flash_unlock_seq()
{
	*(baseaddress + 0x555) = __BE(0xAA);
	*(baseaddress + 0x2AA) = __BE(0x55);
}

static inline void reset(void)
{
	flash_unlock_seq();
	*(baseaddress + 0x000) = __BE(0xF0);
}

/*
 * AMD STANDARD WRITE
 */
void fl_write_word (volatile unsigned short *dest, unsigned short value)
{
	unsigned char ready;

	/*
	 * value = 0x0400; significa D26 (pijn 112) settato su cpu */

	//for (;;) {
	//	*baseaddress = 0x0400;
	//	*baseaddress = 0x0000;
	//}

	// disable interrupts
	asm volatile ("move #0x2700, %sr");

	flash_unlock_seq();

	*(baseaddress + 0x555) = __BE(0xA0);

	*dest = value;

	// re-enable interrupts
	asm volatile ("move #0x2000, %sr");

        // Wait for verification...
	while (1) {
		ready = *dest == value;
		/* Good burn! */
		if (ready) break;

		delay(10);
	}
}

static void flash_erase_status_poll(volatile unsigned short *dest)
{
	unsigned short ready;
	// Wait for verification...
	while (1) {
		ready = *dest == 0xffff;
		/* Good burn! */
		if (ready) break;

		delay(10);
	}
}

void fl_erase_chip ()
{
	// disable interrupts
	asm volatile ("move #0x2700, %sr");

        // erase chip
	flash_unlock_seq();
	*(baseaddress + 0x555) = __BE(0x80);
	flash_unlock_seq();
	*(baseaddress + 0x555) = __BE(0x10);

	// re-enable interrupts
	asm volatile ("move #0x2000, %sr");

	flash_erase_status_poll((volatile unsigned short *)0xffc00000);
}

void fl_erase_block (int block)
{
	unsigned long addr = 0x10000 * block / 2;
	// disable interrupts
	asm volatile ("move #0x2700, %sr");

	// erase block (64K)
	flash_unlock_seq();
	*(baseaddress + 0x555) = __BE(0x80);
	flash_unlock_seq();
	*(baseaddress + addr) = __BE(0x30);

	// re-enable interrupts
	asm volatile ("move #0x2000, %sr");

	flash_erase_status_poll(baseaddress + addr);
}

void fl_write_binary(volatile unsigned short *src,
		     volatile unsigned short *dst, int size)
{
	int i;

	/* reset flash */
	*baseaddress = FLASH_RESET;
	/* write 16bit words */
	size >>= 1;
	while (size--) {
                fl_write_word(dst++, *src++);
        }
	/* back in read mode */
	*(unsigned short*)baseaddress = FLASH_READ_MODE;
}

void fl_init()
{
	baseaddress = (unsigned short *)FLASH_START;

	reset();
}

#ifdef DEBUG_FLASH

/* read reliability test
 * re-read x10 each address
 */
static void dump_check()
{
	volatile unsigned short *fp = (unsigned short *)(FLASH_START);
	volatile unsigned short rd, rt;
	int x, y, i;

	*baseaddress = (unsigned short)FLASH_RESET;

	for (;;) {
		for (y = 0; y < 0x4000; ++y) {
			for (x = 0; x < 8; ++x, fp++) {
				rd = *fp;
				for (i = 0; i < 10; ++i) {
					rt = *fp;
					if (rt != rd) {
						write("\r\n+++ error at ");
						write_hex_l((unsigned long)fp);
						write("\r\nfirst read was ");
						write_hex_s(rd);
						write("\r\nnow read ");
						write_hex_s(rt);
						for (;;);
					}
				}
			}
		}
	}
}

static void erase_and_check(void)
{
	int x;
	volatile unsigned short *p = baseaddress;

	write("erasing chip\r\n");

	fl_erase_chip();

	write("done\r\n");

	for (x = 0; x < (1024 * 1024 * 2); ++x) {
		if (*p++ != 0xffff) {
			write("err @ ");
			write_hex_l((unsigned long)p);
			write("\r\n");
			for (;;);
		}
	}
	write("ok\r\n");
}

#ifdef DEBUG_FLASH_ID
int check_id (unsigned short *mf_id, unsigned short *part_id)
{
	*baseaddress = (unsigned short)FLASH_RESET;

	*(baseaddress + 0x555) = __BE(0xAA);
	*(baseaddress + 0x2AA) = __BE(0x55);
	*(baseaddress + 0x555) = __BE(0x90);

	*mf_id = *(baseaddress);
	*part_id = *(baseaddress + 1);

	*(baseaddress) = (unsigned char)FLASH_READ_MODE;

	return (*mf_id == 0xbf00 && *part_id == 0x5d23) ;
}
#endif

void debug_flash()
{
	unsigned short mf_id = 0, part_id = 0;

	*baseaddress = (unsigned short)FLASH_RESET;

#if defined(DEBUG_FLASH_ID)
	if (check_id (&mf_id, &part_id)) {
		write("id ok\r\n");
	} else {
		write("id err\r\n");
	}
#endif
#if defined(DEBUG_DATA_BUS_SIMPLE)
	/*
	 * This step perform two fake write in a loop, just to generate
	 * a square wave in each data wire.
	 * Check square wave to have a good shape (some time a short gives
	 * a bad shape, still square-similar but clearly not good and stable)
	 * and check square is there on the flash component body pin,
	 * this will give both continuity and no-shorts certainity.
	 */
	write("generic data bus check ...\r\n");

	for (;;) {
		*baseaddress = 0x0000;
		*baseaddress = 0xffff;
	}
#endif

#if defined(DEBUG_ADDR_BUS_SIMPLE)
	/*
	 * This step perform two read in a loop, just to generate
	 * a square wave in each address bus wire.
	 * Check square wave to have a good shape (some time a short gives
	 * a bad shape, still square-similar but clearly not good and stable)
	 * and check square is there on the flash component body pin,
	 * this will give both continuity and no-shorts certainity.
	 */
	write("generic address bus check ...\r\n");

	for (;;) {
		mf_id = *(volatile unsigned short *)(0xffc00000);
		mf_id = *(volatile unsigned short *)(0xffffffff);
	}
#endif

#if defined(DEBUG_FLASH_FULL)
	erase_and_check();
#endif

#if defined(DEBUG_FLASH_WRITE)
	write("generic write check ...\r\n");

	fl_write_word(baseaddress, 0xaaaa);
	if (*baseaddress == 0xaaaa) {
		write("write and read back test passed.\r\n");
	}

#endif

#if defined(DEBUG_FLASH_DUMP)
	*baseaddress = (unsigned short)FLASH_RESET;

	write ("\r\n\r\ndump check (from 0xfffc0000)");
	dump_check();
	write ("\r\n\r\ndump ok\n");
#endif
	for (;;);
}
#endif // DEBUG_FLASH
