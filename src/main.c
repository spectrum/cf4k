/*
 * CF4K bootloader program 30-03-2010
 *
 * This program allow to debug board components and
 * to perform the first flash programming.
 *
 * Sysam (C) 2010 - Angelo Dureghello
 *
 * redesign -> 0.90 16.1.2015
 *
 */

#include "config.h"

#include "arch.h"
#include "serial.h"
#include "memory.h"
#include "serial.h"
#include "vt100.h"
#include "flash.h"

#define VERSION "0.92(alpha) 25102016"

static void hwsetup(void)
{
	/* global chip select init (CS0) */
	*(unsigned short *)MCFSIM_CSAR0 = (FLASH_START >> 16);

	*(unsigned short *)MCFSIM_CSCR0 = 0
				| CS_CSCR_WS(6)
				| CS_CSCR_AA
				| CS_CSCR_PS16;

	*(unsigned long *) MCFSIM_CSMR0 = CS_CSMR_BAM_4M | CS_CSMR_V;

	/* sdram init */
	meminit();

	init_serial(UART_CONSOLE, 115200);
}

#ifdef DEBUG
/*
 * debug mode, hw debug, performances, etc
 * see config.h DEBUG_XX defines
 */
void start_main(void)
{
	hwsetup();

	write("cf4k dbg config\r\n");

	fl_init();

#ifdef DEBUG_FLASH
	debug_flash();
#endif

#ifdef DEBUG_SDRAM
	memtest();
#endif
#ifdef DEBUG_SPEED_TEST
	/* setup for square wave */
	*(unsigned short *)(MCFSIM_PAR)   = 0x300;
	*(unsigned short *)(MCFSIM_PADDR) = 0xfcff;

	asm volatile (
	"start:			\n\t"
	"move.w  #0xAAAA,(%0)	\n\t"
	"move.w  #0x5555,(%0)	\n\t"
	"jmp	 start		\n\t"
	: : "a"(MCFSIM_PADAT) : );
#endif
#ifdef DEBUG_SERIAL
	write("serial port test\r\n");
	continue;
#endif
	write("start_main(): all ok\n\n");
}

#else
/*
 * release mode only
 */
void erase_flash(void)
{
	unsigned short x, y;

	write ("\033[2J\033[HErasing chip ...\r\n");

	fl_erase_chip();

	write ("\033[2J\033[HDone.");
	getchar(0);
}

int get_binary(void)
{
	volatile unsigned char *ptrr = 0;
	int	i = 0, kb = 0, c;

	write ("\033[2J\033[HBinary upload\r\n\r\n");
	write ("Waiting ... \r\n");
	write ("\x1b[s");

	while (i == 0) {
		while ((c = getchar(1)) != GETCHAR_TIMEOUT) {
			ptrr[i++] = c;
			/* no write of info here ! or chars are lost */
		}
	}

	write ("\r\n\r\nfile uploaded successfully\r\n");
	write ("received ");
	writen(i);
	write (" bytes\r\n\r\n");
	write ("write to flash (y/n) ? ");

	char x = getchar(0);

	if (x == 'y') return i;

	write("\r\naborted\r\n");

        getchar(0);

	return 0;
}

/**
 * Erase and write bootloader in first 64K block.
 */
void write_bootloader(void)
{
	int size;

	fl_erase_block(0);
	fl_erase_block(1);
	fl_erase_block(2);

	size = get_binary();

	if (size > 0) {
		fl_write_binary(0, (unsigned short *)FLASH_START, size);
	}
}

#ifdef JFFS2
void write_JFFS2(void)
{
	int x;

	for (x=JFFS_START; x < (JFFS_START + JFFS_BLOCKS); x++)
		fl_erase_block(x);

	x = get_binary();

	fl_write_binary(0, (unsigned short *)
		(FLASH_START + (BLOCK_SIZE * JFFS_START)), x);
}
#endif

/**
 * Erase and write linux from second 64K block.
 * Maximum block are
 */
void write_linux(void)
{
	int x;

	write("erasing\r\n");

	for (x = 2;x < LINUX_BLOCKS; x++) {
		fl_erase_block(x);
		write (".");
	}

	write("\r\nblocks erased\r\n");

	/* reset flash */
	*(volatile unsigned short*)(FLASH_START) = 0xf0f0;

	/* erase check */
	unsigned short q =
		*(volatile unsigned short*)(FLASH_START + (2 * BLOCK_SIZE));

	if (q != 0xffff) {
		write ("\r\nerror !\r\n");
	}

	x = get_binary();

	fl_write_binary(0, (unsigned short *)
			(FLASH_START + (2 * BLOCK_SIZE)), x);
}

void start_main(void)
{
	hwsetup();

	fl_init();

	for (;;) {
		vt100_screen_clear();
		vt100_cursor_pos(2,2);

		vt100_write_color(COLOR_YELLOW, "CF4K bootloader ");
		vt100_write_color(COLOR_FUXIA, VERSION);

		vt100_cursor_pos(2,4);
		vt100_write_color(COLOR_RED, "1 - erase chip");
		vt100_cursor_pos(2,5);
		vt100_write_color(COLOR_NONE, "2 - write bootloader");
		vt100_cursor_pos(2,6);
		vt100_write_color(COLOR_NONE, "3 - write linux");
#ifdef JFFS2
		vt100_cursor_pos(2,7);
		vt100_write_color(COLOR_NONE, "4 - write JFFS2");
#endif
		vt100_cursor_pos(2,9);
		vt100_write_color(COLOR_NONE, "5 - dump");
		vt100_cursor_pos(2,11);
		vt100_write_color(COLOR_NONE, "Enter choice [1 to 4] ");

		switch (getchar(0))
		{
		case 0x31:
		{
			int c;
			vt100_cursor_pos(2,11);
			vt100_write_color(COLOR_RED, "Type <Y> to confirm ...");
			if (getchar(0) == 'Y') {
				erase_flash();
			}
		}
		break;
		case 0x32: write_bootloader(); break;
		case 0x33: write_linux(); break;
#ifdef JFFS2
		case 0x34: write_JFFS2(); break;
#endif
		case 0x35:
		{
			unsigned short *addr = (unsigned short *)0xffc00000;
redump:
			fl_dump(addr, 256);
			switch(getchar(0)) {
			case 'n': addr += 512; goto redump;
			break;
			}
		}
		break;
		default:
		break;
		}
	}
}
#endif /* DEBUG */

