/**
 * memory.c
 * memory test support
 */

#include "memory.h"
#include "arch.h"
#include "serial.h"
#include "timing.h"

#include "config.h"

#define SDRAM_SIZE		(1024*1024*16)
#define SDRAM_SIZE_DW		(SDRAM_SIZE/4)
#define SDRAM_1M_BOUNDARY	(SDRAM_SIZE_DW/16)

void meminit(void)
{
	volatile unsigned long *ram = (unsigned long *)0x00000000;

	delay(0x40000);

	/*
	 * DCR
	 * 0x26 mean Refresh period at 64msec (x4096 rows)
	 * 0x8000,0x8100,0x8200 are all valid, faster is 0x8000 (7G, tRC is >=70ns)
	 */
	*((volatile unsigned short *)(MCFSIM_DCR))	= 0x8226;

	/**
	 * DACR0
	 * page mode on burst only (to be verified) 0x00
	 * CMD on A20 0x0300
	 */
	*((volatile unsigned long  *)(MCFSIM_DACR0)) = 0x00003304;
	*((volatile unsigned long  *)(MCFSIM_DMR0)) = 0x00ff0001;

	/* issue a PRECHARGE ALL */
	*((volatile unsigned long  *)(MCFSIM_DACR0)) = 0x0000330c;
	*((volatile unsigned long  *)(0x00000004)) = 0xbeaddeed;

	*((volatile unsigned long  *)(MCFSIM_DACR0)) = 0x0000b304;

	delay(0x68a);

	*((volatile unsigned long  *)(0x00000004)) = 0xbeaddeed;
	*((volatile unsigned long  *)(MCFSIM_DACR0)) = 0x0000b304;

	delay(0x40000);

	*((volatile unsigned long  *)(MCFSIM_DACR0)) = 0x0000b344;
	*((volatile unsigned long  *)(0x00000c00)) = 0xbeaddeed;

	/* Memory block 1 not in use (DMR1[V] = 0) */
        *((volatile unsigned long *) (MCFSIM_DMR1)) = 0x00040000;
        *((volatile unsigned long *) (MCFSIM_DACR1)) = 0x00000000;
}

#ifdef DEBUG
#ifdef DEBUG_SDRAM

#define MAX_ERRORS  16
static unsigned long err_table[MAX_ERRORS][2];

void display_error(volatile unsigned long *ram,
		   unsigned long expected, unsigned long read)
{
	write("error reading from :");
	write_hex_l((unsigned long)ram);
	write(" read:");
	write_hex_l(read);
	write(" expected:");
	write_hex_l(expected);
	write("\r\n");
	write("note: 0x000000ff means cpu pins D0 to D7 high\r\n");
	write("note: if data line is ok, so it's an address issue\r\n");
}

int memtest()
{
	volatile unsigned long *ram = (unsigned long *)0x00000000;
	unsigned long i, pattern;

	write("\033[2J\033[Hmemtest\r\n\r\n");

	for (i = 0, pattern = 0; i < SDRAM_SIZE_DW; ++i) {
		*ram++ = pattern++;
	}

	write("all written\r\n");

	for (i = 0, ram = 0; i < SDRAM_SIZE_DW; ++i) {
		pattern = *ram++;
		if (pattern != i) {
			display_error(--ram, i, pattern);
			break;
		}

	}

	write("test completed.");
	
	/* let's wait a bit, allowing some ref */
	getchar(0);

}
#endif
#endif
