#ifndef CONFIG_H
#define CONFIG_H

/*
 * Define master clock frequency of our 5307.
 */
#define  MCF_CLK		45000000

/*
 * The board memory map is set up as follows:
 *
 * 0x00000000  --  SDRAM (operational memory - 4 or 16 Mb)2
 * 0x10000000  --  MBAR (5307 SIM module peripherals)
 * 0x20000000  --  RAMBAR (5307 internal SRAM - 4k)
 * 0xffx00000  --  FLASH (XMb) (CS0)
 */

#define SST39VF320xB
#define FLASH_START		0xffc00000

// 4 Kb limit require specialized release / debug configurations
#ifdef DEBUG
/*
 * there is no space for all tests routines, 4K limit,
 * only some tests can be enabled.
 */

/* enable here after-soldergin extended debug */
#define DEBUG_EXTENDED

#ifdef DEBUG_EXTENDED
#define DEBUG_SDRAM
#define DEBUG_SPEED_TEST
#else
#define DEBUG_FLASH
//#define DEBUG_DATA_BUS_SIMPLE
//#define DEBUG_ADDR_BUS_SIMPLE
//#define DEBUG_FLASH_ID
//#define DEBUG_FLASH_FULL
//#define DEBUG_FLASH_WRITE
//#define DEBUG_FLASH_DUMP
#endif

//#define DEBUG_SQUARE
//#define DEBUG_SERIAL
#else
/* linux blocks. leave first 2 blocks to u-boot */
#define LINUX_BLOCKS		(48-2)
/* JFFS2 test, last megabyte, with 1 less block used for bootloader */
//#define JFFS2			1
#define JFFS_START		48
#define JFFS_BLOCKS		16
#endif /* DEBUG */
#endif /* CONFIG_H */
