#ifndef flash_H
#define flash_H

#define SECT_SIZE		1024
#define BLOCK_SIZE		65536

#define FLASH_RESET		0xf0f0
#define FLASH_READ_MODE		0xF0

void debug_flash();

void fl_init();
void fl_erase_init();
void fl_write_word (volatile unsigned short *dest, unsigned short value);
void fl_erase_chip();
void fl_erase_block(int block);
void fl_write_binary(volatile unsigned short *src, volatile unsigned short *dst, int size);
void fl_dump(volatile unsigned short *addr, int size);

#endif
