#ifndef serial_H
#define serial_H

/* console ooutput */
#define UART_CONSOLE		MCFUART_BASE1

#define GETCHAR_TIMEOUT		0xffffffff

void init_serial(unsigned int uart_base, unsigned int speed);
void write_hex_b(unsigned char c);
void write_hex_s(unsigned short s);
void write_hex_l(unsigned long l);
void putchar(unsigned char ch);
void write(unsigned char *p);
void writen(int v);
int getchar(unsigned char timeout);

#endif
