#ifndef vt100_h
#define vt100_h

#ifndef DEBUG
/* vt100 support */
void clear_termianl();

/* letrs' color a bit */
typedef unsigned char* color;

#define COLOR_NONE   "\x1b[0m"
#define COLOR_BLACK  "\x1b[1;30m"
#define COLOR_RED    "\x1b[1;31m"
#define COLOR_GREEN  "\x1b[1;32m"
#define COLOR_YELLOW "\x1b[1;32m"
#define COLOR_BLUE   "\x1b[1;34m"
#define COLOR_FUXIA  "\x1b[1;35m"
#define COLOR_CYAN   "\x1b[1;36m"
#define COLOR_WHITE  "\x1b[1;37m"

void vt100_screen_clear();
void vt100_cursor_home();
void vt100_cursor_pos(int col, int row);
void vt100_write_color(color, unsigned char *);

#endif /* DEBUG */

#endif /* vt100_h */
