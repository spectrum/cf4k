#!/bin/bash

# copy init.gdb as /home/angelo/.gdbinit
if [ ! -f "~/.gdbinit" ]; then
	cp init.gdb ~/.gdbinit
	echo "continue" >> ~/.gdbinit
fi

m68k-elf-gdb
